#include <pluginlib/class_loader.h>
#include <ros/ros.h>
// MoveIt
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_state/conversions.h>
#include <moveit/planning_pipeline/planning_pipeline.h>
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/kinematic_constraints/utils.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_msgs/PlanningScene.h>
#include <moveit_msgs/ExecuteTrajectoryAction.h>
#include <std_msgs/Bool.h>
#include <std_msgs/UInt32.h>
#include <geometry_msgs/Polygon.h>
#include <geometry_msgs/Point32.h>
#include <geometry_msgs/PoseArray.h>

class Planner
{
public:
    Planner(ros::NodeHandle *node_handle_ptr, ros::NodeHandle *node_handle_private_ptr);
    void planningSceneManager(moveit_msgs::PlanningSceneWorld world);
    void pathPlanner(geometry_msgs::PoseArray goal);
    void trajectoryPoints(moveit_msgs::DisplayTrajectory robot_trajectory);
    void executionProcess(moveit_msgs::ExecuteTrajectoryActionResult result);
    void robotCommand(std_msgs::UInt32 command);
    void unityStateUpdate(const ros::TimerEvent &event);
    void checkValidity(const ros::TimerEvent &event);

private:
    const std::string PLANNING_GROUP = "panda_arm"; // Used to be "panda_arm"
    const std::string BASE_FRAME = "panda_link0";   //Used to be "panda_link0"
    const std::string EEF = "panda_link8";
    // Robot
    robot_model_loader::RobotModelLoaderPtr robot_model_loader;
    robot_model::RobotModelPtr robot_model;
    robot_state::RobotStatePtr robot_state_holder;

    const robot_state::JointModelGroup *joint_model_group;

    // Planning scene monitor
    planning_scene_monitor::PlanningSceneMonitorPtr psm;

    // Planner
    planning_pipeline::PlanningPipelinePtr planning_pipeline;
    planning_interface::MotionPlanRequest req;
    planning_interface::MotionPlanResponse res;

    std::vector<double> tolerance_pose;
    std::vector<double> tolerance_angle;

    // Point Array for displaying
    geometry_msgs::PoseArray input_goal;
    moveit_msgs::DisplayTrajectory robot_trajectory;

    // ROS Subscriber/Publisher system

    ros::NodeHandle node_handle_;
    ros::NodeHandle node_handle_private_;

    ros::Subscriber collision_object_manager;
    ros::Subscriber path_planner;
    ros::Subscriber vb_robot_command;

    ros::Publisher planning_scene_diff_publisher;
    ros::Publisher trajectory_preview;
    ros::Publisher unityStatePublisher;
    ros::Publisher trajectory_waypoints;

    ros::Timer check_validity_callback;
    ros::Timer state_callback;

    // ROS Execution, using MoveGroupInterface, with execute() and stop() function
    moveit::planning_interface::MoveGroupInterfacePtr movegroup_ptr;
    ros::Subscriber execution_status;
    bool executing_flag = false;
    bool executing = false;
    bool temp_stop = false;
    bool unreachable = false;
    bool ready_state_flag = false;
    int waiting_time_counter = 0;
    const int WAITING_TIME_MAX = 4;
};
// Constructor
Planner::Planner(ros::NodeHandle *node_handle_ptr, ros::NodeHandle *node_handle_private_ptr) : node_handle_(*node_handle_ptr), node_handle_private_(*node_handle_private_ptr)
{
    // Advertise and subscribe to topics
    planning_scene_diff_publisher = node_handle_.advertise<moveit_msgs::PlanningScene>("planning_scene", 1);
    trajectory_preview = node_handle_.advertise<moveit_msgs::DisplayTrajectory>("display_trajectory", 1, true);
    unityStatePublisher = node_handle_.advertise<std_msgs::UInt32>("system_state", 1, true);
    trajectory_waypoints = node_handle_.advertise<geometry_msgs::PoseArray>("trajectory_waypoints", 1, true);

    check_validity_callback = node_handle_.createTimer(ros::Duration(1), &Planner::checkValidity, this);
    state_callback = node_handle_.createTimer(ros::Duration(0.5), &Planner::unityStateUpdate, this);

    collision_object_manager = node_handle_.subscribe("collision_object_manager", 1000, &Planner::planningSceneManager, this);
    path_planner = node_handle_.subscribe("planning_path", 1, &Planner::pathPlanner, this);
    vb_robot_command = node_handle_.subscribe("vb_robot_command", 1, &Planner::robotCommand, this);
    execution_status = node_handle_.subscribe("/execute_trajectory/result", 1, &Planner::executionProcess, this);

    // Load robot model and planning scene with the robot model in it

    robot_model_loader.reset(new robot_model_loader::RobotModelLoader("robot_description"));
    psm.reset(new planning_scene_monitor::PlanningSceneMonitor(robot_model_loader));

    psm->startSceneMonitor();
    psm->startWorldGeometryMonitor();
    psm->startStateMonitor();

    robot_model = robot_model_loader->getModel();
    robot_state_holder.reset(new robot_state::RobotState(planning_scene_monitor::LockedPlanningSceneRO(psm)->getCurrentState()));
    joint_model_group = robot_state_holder->getJointModelGroup(PLANNING_GROUP);

    // Load planning pipeline
    planning_pipeline.reset(new planning_pipeline::PlanningPipeline(robot_model, node_handle_private_, "planning_plugin", "request_adapters"));

    // Setup Planning group for the robot
    req.group_name = PLANNING_GROUP;

    // Setup Planner Tolerance
    for (int i = 0; i < 3; i++)
    {
        tolerance_angle.push_back(0.005);
        tolerance_pose.push_back(0.005);
    }

    // Setup speed
    req.max_velocity_scaling_factor = 0.2;
    req.max_acceleration_scaling_factor = 0.2;

    // Setup timeout for planner
    req.allowed_planning_time = 0.1f;

    // Setup robot controller using MoveGroup
    movegroup_ptr.reset(new moveit::planning_interface::MoveGroupInterface(PLANNING_GROUP));
}
// Adding/Removing CollisionObjects onto the scene, also check for replan if neccessary
void Planner::planningSceneManager(moveit_msgs::PlanningSceneWorld world)
{
    planning_scene_monitor::LockedPlanningSceneRW(psm)->removeAllCollisionObjects();
    bool success = planning_scene_monitor::LockedPlanningSceneRW(psm)->processPlanningSceneWorldMsg(world);
    // Update the scene in Rviz
    moveit_msgs::PlanningScene scene;
    planning_scene_monitor::LockedPlanningSceneRO(psm)->getPlanningSceneMsg(scene);
    planning_scene_diff_publisher.publish(scene);
}
// PathPlanner using motion_planning_api
void Planner::pathPlanner(geometry_msgs::PoseArray goal)
{
    // Save the input path
    input_goal = goal;
    // Clear the result from the previous request
    robot_trajectory.trajectory.clear();

    // Update the latest robot state
    robot_state_holder.reset(new robot_state::RobotState(planning_scene_monitor::LockedPlanningSceneRO(psm)->getCurrentState()));

    // Start planning
    // ^^^^^^^^^^^^^^
    geometry_msgs::PoseStamped pose;
    pose.header.frame_id = BASE_FRAME;
    pose.pose = input_goal.poses.back(); // Only one pose sent per PoseArray Request
    // Set the current robot state as a plan request's start state
    robot_state::robotStateToRobotStateMsg(*robot_state_holder, req.start_state);
    moveit_msgs::Constraints pose_goal =
        kinematic_constraints::constructGoalConstraints(EEF, pose, tolerance_pose, tolerance_angle);
    req.goal_constraints.clear();
    req.goal_constraints.push_back(pose_goal);
    // Impose path constrains to make the EEF parallel to the workspace during the execution
    req.path_constraints.orientation_constraints = pose_goal.orientation_constraints;
    planning_scene_monitor::LockedPlanningSceneRO lscene(psm);
    planning_pipeline->generatePlan(lscene, req, res);
    if (res.error_code_.val != res.error_code_.SUCCESS)
    {
        unreachable = true;
        ROS_ERROR("Could not compute plan sucessfully, error code: %d", res.error_code_.val);
    }
    else
    {
        unreachable = false;
        ROS_INFO("Plan successfully");
        moveit_msgs::MotionPlanResponse response;
        res.getMessage(response);
        robot_trajectory.trajectory_start = response.trajectory_start;
        robot_trajectory.trajectory.push_back(response.trajectory);

        // Update the trajectory line
        trajectoryPoints(robot_trajectory);
        trajectory_preview.publish(robot_trajectory);
    }
    ROS_INFO("Finish Planning");
}

// Execution Handler
void Planner::executionProcess(moveit_msgs::ExecuteTrajectoryActionResult result)
{
    if (result.result.error_code.val == moveit_msgs::MoveItErrorCodes::SUCCESS)
    {
        // Clear the already execute trajectory
        robot_trajectory.trajectory.clear();
        ROS_INFO("Trajectory Successfully Executed");
        ROS_INFO("Waiting on the next request");
        executing = false;
        executing_flag = false;
    }
    else if (result.result.error_code.val == result.result.error_code.PREEMPTED)
    {
        ROS_INFO("Trajectory Terminated");
    }
    else
    {
        ROS_INFO("Execution Failed. Error Code %d", result.result.error_code.val);
    }
}
// Main loop, check the validity of each path and decides the corresponding action
void Planner::checkValidity(const ros::TimerEvent &event)
{
    std::vector<std::size_t> idx;
    // Check if the executing path is valid, only when there is a path to check
    if (robot_trajectory.trajectory.size())
    {
        bool valid = planning_scene_monitor::LockedPlanningSceneRO(psm)->isPathValid(robot_trajectory.trajectory_start, robot_trajectory.trajectory.back(), PLANNING_GROUP, false, &idx);
        // If the trajectory is invalid
        if (!valid)
        {
            // And the robot is executing that trajectory
            if (executing_flag && executing)
            {
                // Stop the trajectory
                std_msgs::UInt32 command;
                command.data = 2;
                robotCommand(command);
                executing = false;
            }
            else
            {
                // Just waiting for the user to get out
                return;
            }
        }
        // If the trajectory is valid
        else
        {
            // And the robot is not executing any trajectory
            if (executing_flag && !executing)
            {
                if (waiting_time_counter == WAITING_TIME_MAX) // WAITING_TIME_MAX is set to 4, meaning after 4 callback (0.5 x 4 = 2s), the trajectory would be replanned
                {
                    // Replan the trajectory to reach the goal
                    pathPlanner(input_goal);
                    // And execute that trajectory
                    std_msgs::UInt32 command;
                    command.data = 1;
                    robotCommand(command);
                    executing = true;
                    // Reset the waiting time counter
                    waiting_time_counter = 0;
                }
                else
                { // If the waiting is not yet timeout, just keep waiting
                    waiting_time_counter++;
                    ROS_INFO("Waiting for 2s before replanning");
                    return;
                }
            }
            else
            {
                // Just let the robot executing its trajectory
                return;
            }
        }
    }
}

void Planner::unityStateUpdate(const ros::TimerEvent &event)
{
    static const uint32_t IDLE = 1;
    static const uint32_t EXECUTING = 2;
    static const uint32_t TEMP_STOP = 3;
    static const uint32_t MOVE_READY_STATE = 4;
    static const uint32_t PREVIEW_SHOWING = 5;
    static const uint32_t UNREACHABLE = 6;
    std_msgs::UInt32 unity_state;
    if (executing_flag && executing) // Change the condition here
    {
        unity_state.data = EXECUTING;
    }
    else if (executing_flag && !executing) // Change the condition here
    {
        unity_state.data = TEMP_STOP;
    }
    else if (ready_state_flag) // Change the condition here
    {
        unity_state.data = MOVE_READY_STATE;
    }
    else if (unreachable) // If barrier is in the way, cannot plan to goal
    {
        unity_state.data = UNREACHABLE;
    }
    else
    {
        unity_state.data = IDLE;
    }
    unityStatePublisher.publish(unity_state);
}

void Planner::robotCommand(std_msgs::UInt32 command)
{
    static const uint32_t EXECUTING = 1;
    static const uint32_t STOP = 2;
    static const uint32_t READY_STATE = 3;
    switch (command.data)
    {
    case EXECUTING:
        if (robot_trajectory.trajectory.size() > 0 && !executing_flag)
        {
            ROS_INFO("Execution Permission granted");
            executing_flag = true;
            movegroup_ptr->asyncExecute(robot_trajectory.trajectory.front());
            executing = true;
        }
        else
        {
            ROS_INFO("No trajectory has been added for execution");
        }
        break;
    case STOP:
        ROS_INFO("Robot termination");
        movegroup_ptr->stop();
        break;
    case READY_STATE:
        ROS_INFO("Bringing back to ready state");
        // First check if the robot is still running
        if (executing)
        {
            ROS_INFO("Robot is executing another trajectory, try later");
            return;
        }
        else
        {
            robot_state::RobotState ready_goal_state = robot_state::RobotState(planning_scene_monitor::LockedPlanningSceneRO(psm)->getCurrentState());
            ready_goal_state.setToDefaultValues(joint_model_group, "ready");
            robot_state::robotStateToRobotStateMsg(robot_state::RobotState(planning_scene_monitor::LockedPlanningSceneRO(psm)->getCurrentState()), req.start_state);
            moveit_msgs::Constraints state_goal =
                kinematic_constraints::constructGoalConstraints(ready_goal_state, joint_model_group);
            req.goal_constraints.clear();
            req.goal_constraints.push_back(state_goal);
            planning_scene_monitor::LockedPlanningSceneRO lscene(psm);
            planning_pipeline->generatePlan(lscene, req, res);
            if (res.error_code_.val != res.error_code_.SUCCESS)
            {
                ROS_INFO("Plan bringing back to ready state: Unsucessful, error code: %d", res.error_code_.val);
            }
            else
            {
                ROS_INFO("Plan bringing back to ready state: Sucessful");
                moveit_msgs::MotionPlanResponse response;
                res.getMessage(response);
                robot_trajectory.trajectory_start = response.trajectory_start;
                robot_trajectory.trajectory.push_back(response.trajectory);
                std_msgs::UInt32 command;
                command.data = EXECUTING;
                robotCommand(command);
                ROS_INFO("Bring back to ready state request sent");
            }
        }
        break;
    default:
        ROS_INFO("Command not found.");
        break;
    }
}
void Planner::trajectoryPoints(moveit_msgs::DisplayTrajectory robot_trajectory)
{
    geometry_msgs::PoseArray traj_points;
    for (int i = 0; i < robot_trajectory.trajectory.size(); i++)
    {
        for (int j = 0; j < robot_trajectory.trajectory[i].joint_trajectory.points.size(); j++)
        {
            robot_state_holder->setJointGroupPositions(joint_model_group, robot_trajectory.trajectory[i].joint_trajectory.points[j].positions);
            const Eigen::Isometry3d &end_effector_state = robot_state_holder->getGlobalLinkTransform(EEF);
            geometry_msgs::Pose traj_point;
            traj_point.position.x = end_effector_state.translation()[0];
            traj_point.position.y = end_effector_state.translation()[1];
            traj_point.position.z = end_effector_state.translation()[2];
            traj_points.poses.push_back(traj_point);
        }
    }
    trajectory_waypoints.publish(traj_points);
}
int main(int argc, char **argv)
{
    ros::init(argc, argv, "planner_test");
    ros::AsyncSpinner spinner(4);
    spinner.start();
    ros::NodeHandle node_handle("");
    ros::NodeHandle node_handle_private("~");
    Planner myPlanner(&node_handle, &node_handle_private);
    ros::waitForShutdown();
    return 0;
}

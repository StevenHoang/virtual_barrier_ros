#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>

#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>

#include <moveit_visual_tools/moveit_visual_tools.h>
#include <math.h>

int main(int argc, char** argv){

    ros::init(argc, argv, "motion_test");
    ros::NodeHandle node_handle;
    ros::AsyncSpinner spinner(1);
    spinner.start();

    //Setup
    static const std::string PLANNING_GROUP = "panda_arm";
    moveit::planning_interface::MoveGroupInterface move_group(PLANNING_GROUP);
    moveit::planning_interface::PlanningSceneInterface planning_scene_interface;    
    moveit_visual_tools::MoveItVisualTools visual_tools("panda_link0");
    const robot_state::JointModelGroup* joint_model_group =
            move_group.getCurrentState()->getJointModelGroup(PLANNING_GROUP);

    visual_tools.deleteAllMarkers();
    moveit::planning_interface::MoveGroupInterface::Plan my_plan;    
    moveit::core::RobotStatePtr current_state = move_group.getCurrentState();

    std::vector<double> joint_group_position;

    current_state->copyJointGroupPositions(joint_model_group, joint_group_position);

    // Ready-state: information retrieved from panda_moveit_config/config/panda_arm.xacro
    joint_group_position[0] = 0;
    joint_group_position[1] = -M_PI_4;
    joint_group_position[2] = 0;
    joint_group_position[3] = -3*M_PI_4;
    joint_group_position[4] = 0;
    joint_group_position[5] = -M_PI_2;
    joint_group_position[6] = M_PI_4;

    // Test zero_angle state
    for (int i = 0; i < joint_group_position.size(); i++){
        joint_group_position[i] = 0;
    }
    // The joint posision of panda_joint6 is set at pi in order to avoid
    // collision of joint_7 and joint_5 (this is only applied for Panda)
    joint_group_position[5] = M_PI;

    move_group.setJointValueTarget(joint_group_position);
    bool success = (move_group.plan(my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
    ROS_INFO("Move to zero state: %d", success);
    move_group.execute(my_plan); // Replacement for move_group.move()
    ros::shutdown();
    return 0;
}

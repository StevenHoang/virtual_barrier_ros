#include <pluginlib/class_loader.h>
#include <ros/ros.h>
// MoveIt
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_state/conversions.h>
#include <moveit/planning_pipeline/planning_pipeline.h>
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>
#include <moveit_visual_tools/moveit_visual_tools.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/kinematic_constraints/utils.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_msgs/PlanningScene.h>
#include <moveit_msgs/ExecuteTrajectoryAction.h>
#include <std_msgs/Bool.h>
#include <std_msgs/UInt32.h>
#include <geometry_msgs/Polygon.h>
#include <geometry_msgs/Point32.h>
#include <geometry_msgs/PoseArray.h>

class Planner
{
public:
    Planner(ros::NodeHandle *node_handle_ptr, ros::NodeHandle *node_handle_private_ptr);
    void planningSceneManager(moveit_msgs::PlanningScene scene);
    void pathPlanner(geometry_msgs::PoseArray path);
    void trajectoryDisplay(moveit_msgs::DisplayTrajectory robot_trajectory);
    void robotCommand(std_msgs::UInt32 command);
    void executionProcess(moveit_msgs::ExecuteTrajectoryActionResult result);
    void replan();
    void unityStateUpdate(const ros::TimerEvent &event);
    bool checkValidity();

    // Helper function
    std::vector<int> vectorsort(std::vector<int> vector);

private:
    const std::string PLANNING_GROUP = "panda_arm"; // Used to be "panda_arm"
    const std::string BASE_FRAME = "panda_link0";   //Used to be "panda_link0"
    const std::string EEF = "panda_link8";
    // Robot
    robot_model_loader::RobotModelLoaderPtr robot_model_loader;
    robot_model::RobotModelPtr robot_model;
    robot_state::RobotStatePtr robot_state_holder;
    robot_state::RobotStatePtr forward_kinematic_solver;
    std::vector<moveit_msgs::RobotState> traj_start_state_msg; // Start state robot_state_msgs for reference
    std::vector<robot_state::RobotState> waypoint_robot_state; // Robot state at each waypoint, including the starting and ending waypoints

    const robot_state::JointModelGroup *joint_model_group;
    std::vector<std::vector<std::size_t>> index;
    std::vector<int> classification;

    // Planning scene monitor
    planning_scene_monitor::PlanningSceneMonitorPtr psm;
    bool planning = false;
    int plan_delay = 0;
    int replan_attempts_count = 0;
    const int REPLAN_FAIL_MAX = 3;
    bool replan_flag = false;

    // Planner
    planning_pipeline::PlanningPipelinePtr planning_pipeline;
    planning_interface::MotionPlanRequest req;
    planning_interface::MotionPlanResponse res;

    std::vector<double> tolerance_pose;
    std::vector<double> tolerance_angle;

    // Saved goalpoints array
    geometry_msgs::PoseArray input_path;
    // Divided into stages
    std::vector<geometry_msgs::PoseArray> stages;
    // The set of goalpoints that the robot is executing
    geometry_msgs::PoseArray executing_path;
    // Trajectory(es) for executing
    moveit_msgs::DisplayTrajectory robot_trajectory;

    // Display
    moveit_visual_tools::MoveItVisualToolsPtr moveit_visual_tools_ptr_;
    moveit_msgs::CollisionObject workspace;
    // ROS Subscriber/Publisher system
    ros::NodeHandle node_handle_;
    ros::NodeHandle node_handle_private_;

    ros::Subscriber collision_object_manager;
    ros::Subscriber path_planner;
    ros::Subscriber vb_robot_command;

    ros::Publisher trajectory_preview;
    ros::Publisher unityStatePublisher;
    ros::Publisher trajectory_waypoints;

    ros::Timer replan_callback;
    float replan_frequency = 1.0f;
    ros::Timer state_callback;
    float state_publish_frequency = 0.5f;

    // ROS Execution, using MoveGroupInterface, with execute() and stop() function
    moveit::planning_interface::MoveGroupInterfacePtr movegroup_ptr;
    ros::Subscriber execution_status;
    bool execution_flag = false;
    bool executing = false;
    bool temp_stop = false;
    bool ready_state_flag = false;
};
// Constructor
Planner::Planner(ros::NodeHandle *node_handle_ptr, ros::NodeHandle *node_handle_private_ptr) : node_handle_(*node_handle_ptr), node_handle_private_(*node_handle_private_ptr)
{
    // Advertise and subscribe to topics
    trajectory_preview = node_handle_.advertise<moveit_msgs::DisplayTrajectory>("display_trajectory", 1, true);
    trajectory_waypoints = node_handle_.advertise<geometry_msgs::PoseArray>("trajectory_waypoints", 1, true);
    unityStatePublisher = node_handle_.advertise<std_msgs::UInt32>("system_state", 1, true);

    state_callback = node_handle_.createTimer(ros::Duration(state_publish_frequency), &Planner::unityStateUpdate, this);

    collision_object_manager = node_handle_.subscribe("/planning_scene", 1000, &Planner::planningSceneManager, this);
    path_planner = node_handle_.subscribe("planning_path", 1, &Planner::pathPlanner, this);
    vb_robot_command = node_handle_.subscribe("/vb_robot_command", 1, &Planner::robotCommand, this);
    execution_status = node_handle_.subscribe("/execute_trajectory/result", 1, &Planner::executionProcess, this);

    // Load robot model and planning scene with the robot model in it

    robot_model_loader.reset(new robot_model_loader::RobotModelLoader("robot_description"));
    psm.reset(new planning_scene_monitor::PlanningSceneMonitor(robot_model_loader));

    psm->startSceneMonitor();
    psm->startWorldGeometryMonitor();
    psm->startStateMonitor();

    robot_model = robot_model_loader->getModel();
    robot_state_holder.reset(new robot_state::RobotState(planning_scene_monitor::LockedPlanningSceneRO(psm)->getCurrentState()));
    joint_model_group = robot_state_holder->getJointModelGroup(PLANNING_GROUP);

    // Using RobotState as a forward kinematic solver
    forward_kinematic_solver.reset(new robot_state::RobotState(planning_scene_monitor::LockedPlanningSceneRO(psm)->getCurrentState()));
    // Load planning pipeline
    planning_pipeline.reset(new planning_pipeline::PlanningPipeline(robot_model, node_handle_private_, "planning_plugin", "request_adapters"));

    // Setup Planning group for the robot
    req.group_name = PLANNING_GROUP;

    // Setup Planner Tolarence
    for (int i = 0; i < 3; i++)
    {
        tolerance_angle.push_back(0.01);
        tolerance_pose.push_back(0.01);
    }
    // Setup speed
    req.max_velocity_scaling_factor = 0.1;
    req.max_acceleration_scaling_factor = 0.1;

    // Setup timeout for planner
    req.allowed_planning_time = 0.5f;

    // Setup robot controller using MoveGroup
    movegroup_ptr.reset(new moveit::planning_interface::MoveGroupInterface(PLANNING_GROUP));

    // Setup MoveItVisualTools
    moveit_visual_tools_ptr_.reset(new moveit_visual_tools::MoveItVisualTools(BASE_FRAME));
    moveit_visual_tools_ptr_->enableBatchPublishing();
    // Workspace
    workspace.id = "Workspace";
    workspace.header.frame_id = "world";
    shape_msgs::SolidPrimitive workspace_sp;
    workspace_sp.type = workspace_sp.BOX;
    workspace_sp.dimensions.resize(3);
    workspace_sp.dimensions[0] = 1.8;
    workspace_sp.dimensions[1] = 1.25;
    workspace_sp.dimensions[2] = 0.02;

    geometry_msgs::Pose workspace_pose;
    workspace_pose.orientation.w = 1.0;
    workspace_pose.position.x = 0.1;
    workspace_pose.position.y = 0;
    workspace_pose.position.z = -0.01;
    workspace.primitives.push_back(workspace_sp);
    workspace.primitive_poses.push_back(workspace_pose);
    workspace.operation = workspace.ADD;

    planning_scene_monitor::LockedPlanningSceneRW(psm)->processCollisionObjectMsg(workspace);

    // Add workspace onto Rviz
    visualization_msgs::Marker table;
    table.type = table.CUBE;
    table.header.frame_id = "world";
    table.header.stamp = ros::Time();
    table.ns = "my_namespace";
    table.action = table.ADD;
    table.id = '1';
    table.pose.position.x = 0.05;
    table.pose.position.y = 0.0;
    table.pose.position.z = -0.01;
    table.pose.orientation.x = 0.0;
    table.pose.orientation.y = 0.0;
    table.pose.orientation.z = 0.0;
    table.pose.orientation.w = 1.0;
    table.scale.x = 1.8;
    table.scale.y = 1.25;
    table.scale.z = 0.02;
    table.color.a = 1.0;
    table.color.r = 1.0;
    table.color.g = 1.0;
    table.color.b = 1.0;

    visualization_msgs::Marker area;
    area.type = area.CYLINDER;
    area.header.frame_id = "world";
    area.header.stamp = ros::Time();
    area.ns = "my_namespace";
    table.action = area.ADD;
    table.id = '2';
    area.pose.position.x = 0.0;
    area.pose.position.y = 0.0;
    area.pose.position.z = 0.0;
    area.pose.orientation.x = 0.0;
    area.pose.orientation.y = 0.0;
    area.pose.orientation.z = 0.0;
    area.pose.orientation.w = 1.0;
    area.scale.x = 1.6;
    area.scale.y = 1.49;
    area.scale.z = 0.05;
    area.color.a = 1.0;
    area.color.r = 0.0;
    area.color.g = 1.0;
    area.color.b = 1.0;
    moveit_visual_tools_ptr_->publishMarker(table);
    moveit_visual_tools_ptr_->publishMarker(area);
    moveit_visual_tools_ptr_->trigger();
}
// Adding/Removing CollisionObjects onto the scene, also check for replan if neccessary
void Planner::planningSceneManager(moveit_msgs::PlanningScene scene)
{
    if (!planning) // Scene should be locked (not updated) when planning is carried out
    {
        planning_scene_monitor::LockedPlanningSceneRW(psm)->removeAllCollisionObjects();
        // Add collision workspace
        scene.world.collision_objects.push_back(workspace);
        bool success = planning_scene_monitor::LockedPlanningSceneRW(psm)->processPlanningSceneWorldMsg(scene.world);
        replan();
    }
}
// PathPlanner using motion_planning_api
void Planner::pathPlanner(geometry_msgs::PoseArray path)
{
    // If the new request is sent from the client (meaning all the stages in the client's goalpoints array are done)
    if (stages.empty())
    {
        // Save the input path
        input_path = path;
        geometry_msgs::PoseArray stage;
        for (int i = 0; i < input_path.poses.size(); i++)
        {
            stage.poses.push_back(input_path.poses[i]);
            if (input_path.poses[i].position.x < 0 || i == input_path.poses.size() - 1)
            {
                stages.push_back(stage);
                stage.poses.clear();
            }
        }
    }
    // Assign the first stage to be planned
    executing_path = stages.front();
    moveit_msgs::PlanningScene scene;
    waypoint_robot_state.clear();
    robot_trajectory.trajectory.clear();
    planning = true;
    // Start planning
    // ^^^^^^^^^^^^^^
    // Save the starting Robot State
    waypoint_robot_state.push_back(robot_state::RobotState(planning_scene_monitor::LockedPlanningSceneRO(psm)->getCurrentState()));
    moveit_msgs::DisplayTrajectory planner_output_robot_trajectory;
    std::vector<moveit_msgs::RobotState> planner_output_traj_start_state_msg;
    std::vector<robot_state::RobotState> planner_output_waypoint_robot_state = waypoint_robot_state;
    for (int i = 0; i < executing_path.poses.size(); i++)
    {
        geometry_msgs::PoseStamped pose;
        pose.header.frame_id = BASE_FRAME;
        pose.pose = executing_path.poses[i];
        // Set the current robot state as a plan request's start state
        robot_state::robotStateToRobotStateMsg(planner_output_waypoint_robot_state[i], req.start_state);
        moveit_msgs::Constraints pose_goal =
            kinematic_constraints::constructGoalConstraints(EEF, pose, tolerance_pose, tolerance_angle);
        req.goal_constraints.clear();
        req.goal_constraints.push_back(pose_goal);
        planning_scene_monitor::LockedPlanningSceneRO lscene(psm);
        planning_pipeline->generatePlan(lscene, req, res);
        if (res.error_code_.val != res.error_code_.SUCCESS)
        {
            ROS_ERROR("Could not compute plan sucessfully, error code: %d", res.error_code_.val);
        }
        else
        {
            moveit_msgs::MotionPlanResponse response;
            res.getMessage(response);
            planner_output_robot_trajectory.trajectory.push_back(response.trajectory);
            // Save the start state as a message for reference
            planner_output_traj_start_state_msg.push_back(response.trajectory_start);
            // Update the robot state
            robot_state_holder = lscene->getCurrentStateUpdated(response.trajectory_start);
            robot_state_holder->setJointGroupPositions(joint_model_group, response.trajectory.joint_trajectory.points.back().positions);
            planner_output_waypoint_robot_state.push_back(*robot_state_holder);
            ROS_INFO("Plan successfully");
        }
    }
    ROS_INFO("Finish Planning");
    robot_trajectory = planner_output_robot_trajectory;
    waypoint_robot_state = planner_output_waypoint_robot_state;
    traj_start_state_msg = planner_output_traj_start_state_msg;
    planning = false;
    // Update the trajectory line
    trajectoryDisplay(robot_trajectory);
    trajectory_preview.publish(robot_trajectory);
}
bool Planner::checkValidity()
{
    index.clear();
    classification.clear();
    std::vector<std::size_t> idx;
    bool need_replan = false;
    for (int i = 0; i < robot_trajectory.trajectory.size(); i++)
    {
        bool valid = planning_scene_monitor::LockedPlanningSceneRO(psm)->isPathValid(traj_start_state_msg[i], robot_trajectory.trajectory[i], PLANNING_GROUP, false, &idx);
        // Classification of invalid path
        if (!valid)
        {
            // If the trajectory the robot executing is in collision, stop the robot
            if (i == 0 && executing)
            {
                ROS_WARN("Terminate the robot");
                movegroup_ptr->stop();
                executing = false;
                temp_stop = true;
            }
            need_replan = true;
            index.push_back(idx);
            // Path is invalid at the starting state and the finishing state
            if (idx[0] == 0 && (idx[idx.size() - 1] == robot_trajectory.trajectory[i].joint_trajectory.points.size() - 1))
            {
                classification.push_back(1);
            }
            // Path is invalid at the starting state
            else if (idx[0] == 0)
            {
                classification.push_back(2);
            }
            // Path is invalid at the finishing state
            else if (idx[idx.size() - 1] == robot_trajectory.trajectory[i].joint_trajectory.points.size() - 1)
            {
                classification.push_back(3);
            }
            // None of the above (could be invalid but in the middle of the path)
            else
            {
                classification.push_back(4);
            }
        }
        // if path is valid, set 0
        else
        {
            classification.push_back(0);
            std::vector<std::size_t> rd;
            rd.push_back(0);
            index.push_back(rd);
        }
    }
    if (need_replan)
    {
        ROS_INFO("Replan required");
    }
    return !need_replan;
}
void Planner::replan()
{
    /*static int debug_count = 0;
    if (debug_count == 20)
    {
        debug_count = 0;
        ROS_INFO("Number of replan queries: %d", replan_queries_count);
    }
    else
    {
        debug_count++;
    }*/
    bool replan_failed = false;
    if (!planning) // Only runs when nothing is running to avoid conflicting with others
    {
        if (!checkValidity())
        {
            // Replan the path where there are collisions
            // Start planning
            // ^^^^^^^^^^^^^^
            planning = true;
            req.group_name = PLANNING_GROUP;
            while (temp_stop && execution_flag)
            {
                // Wait for robot's feedback
                ROS_INFO("Wait for robot to stop");
            }
            // Get the order of which path to replan first
            // Ascending order (from the lowest class to the highest class)
            std::vector<int> order = vectorsort(classification);

            // std::string str = "";
            // std::string str2 = "";
            // for (int i = 0; i < order.size(); i++)
            // {
            //     str += std::to_string(order[i]);
            //     str += ", ";
            //     str2 += std::to_string(classification[order[i]]);
            //     str2 += ", ";
            // }
            // ROS_INFO("%s", str.c_str());
            // ROS_INFO("%s", str2.c_str());

            // Placeholders for replan function
            std::vector<moveit_msgs::RobotState> replan_traj_start_state_msg(traj_start_state_msg.begin(), traj_start_state_msg.end());
            std::vector<robot_state::RobotState> replan_waypoint_robot_state(waypoint_robot_state.begin(), waypoint_robot_state.end());
            moveit_msgs::DisplayTrajectory replan_robot_trajectory = robot_trajectory;

            // Check for every invalid path
            for (int i = order.size() - 1; i >= 0; i--)
            {
                moveit_msgs::MotionPlanResponse response;
                // Four cases:
                // 1. Both first and last waypoints are in collision
                // 2. Only first waypoint is in collision
                // 3. Only last waypoint is in collision
                // 4. None of the above
                //ROS_INFO("Planning: %d", order[i]);
                // Case 1 and 3
                if (classification[order[i]] == 1 || classification[order[i]] == 3)
                {
                    // Plan to return to the old pose with valid RobotState
                    // First set the requested starting state to be the start of the trajectory
                    robot_state::robotStateToRobotStateMsg(replan_waypoint_robot_state[order[i]], req.start_state);
                    geometry_msgs::PoseStamped pose;
                    pose.header.frame_id = BASE_FRAME;
                    pose.pose = executing_path.poses[order[i]];
                    moveit_msgs::Constraints pose_goal =
                        kinematic_constraints::constructGoalConstraints(EEF, pose, tolerance_pose, tolerance_angle);
                    req.goal_constraints.clear();
                    req.goal_constraints.push_back(pose_goal);
                    planning_scene_monitor::LockedPlanningSceneRO lscene(psm);
                    planning_pipeline->generatePlan(lscene, req, res);
                    if (res.error_code_.val != res.error_code_.SUCCESS)
                    {
                        ROS_ERROR("Traj %d, case %d not sucessfully, error code: %d", order[i], classification[order[i]], res.error_code_.val);
                        planning = false;
                        return;
                    }
                    else
                    {
                        ROS_INFO("Traj %d, case %d successfully", order[i], classification[order[i]]);
                        res.getMessage(response);
                        replan_traj_start_state_msg[order[i]] = response.trajectory_start;
                        replan_robot_trajectory.trajectory[order[i]] = response.trajectory;
                        // Since new pose is now in new RobotState, also update the final state
                        replan_waypoint_robot_state[order[i] + 1].setJointGroupPositions(joint_model_group, response.trajectory.joint_trajectory.points.back().positions);
                    }
                }
                // Case 2 and 4:
                else if (classification[order[i]] == 2 || classification[order[i]] == 4)
                {
                    // Set the requested starting state of the new trajectory to be the starting state of this trajectory
                    robot_state::robotStateToRobotStateMsg(replan_waypoint_robot_state[order[i]], req.start_state);
                    // Bring back to old state
                    moveit_msgs::Constraints state_goal =
                        kinematic_constraints::constructGoalConstraints(replan_waypoint_robot_state[order[i] + 1], joint_model_group);
                    req.goal_constraints.clear();
                    req.goal_constraints.push_back(state_goal);
                    planning_scene_monitor::LockedPlanningSceneRO lscene(psm);
                    planning_pipeline->generatePlan(lscene, req, res);
                    if (res.error_code_.val != res.error_code_.SUCCESS)
                    {
                        ROS_ERROR("Traj %d, case %d not sucessfully, error code: %d", order[i], classification[order[i]], res.error_code_.val);
                        planning = false;
                        return;
                    }
                    else
                    {
                        ROS_INFO("Traj %d, case %d successfully", order[i], classification[order[i]]);
                        res.getMessage(response);
                        replan_traj_start_state_msg[order[i]] = response.trajectory_start;
                        replan_robot_trajectory.trajectory[order[i]] = response.trajectory;
                    }
                }
            }
            // Update the trajectory line
            waypoint_robot_state = replan_waypoint_robot_state;
            traj_start_state_msg = replan_traj_start_state_msg;
            robot_trajectory = replan_robot_trajectory;
            trajectoryDisplay(robot_trajectory);
            trajectory_preview.publish(robot_trajectory);
            planning = false;
            if (execution_flag)
            {
                if (!executing)
                {
                    ROS_INFO("Restart the execution");
                    movegroup_ptr->asyncExecute(robot_trajectory.trajectory.front());
                    executing = true;
                }
                else
                {
                    ROS_INFO("No need to stop the trajectory");
                }
            }
            return;
        }
        else
        {
            // ROS_INFO("No need replan");
            return;
        }
    }
    else
    {
        return;
    }
}
void Planner::executionProcess(moveit_msgs::ExecuteTrajectoryActionResult result)
{
    static float prev_time = 0;
    switch (result.result.error_code.val)
    {
    case moveit_msgs::MoveItErrorCodes::PREEMPTED:
        ROS_WARN("Robot Terminated");
        waypoint_robot_state.front() = robot_state::RobotState(planning_scene_monitor::LockedPlanningSceneRO(psm)->getCurrentState());
        temp_stop = false;
        ROS_INFO("Waypoint Robot State updated for replanning");
        break;
    case moveit_msgs::MoveItErrorCodes::SUCCESS:
        // Wait until replan finishes to get the most updated path
        while (planning)
        {
            // If the waiting time exceeding replan frequency, must meen replan is timed out
            if (ros::Time::now().toSec() - prev_time > replan_frequency)
            {
                // Force timeout on replan function
                ROS_INFO("Replan timeout");
                planning = false;
                prev_time = ros::Time::now().toSec();
                break;
            }
            // Otherwise, just update the user every 0.2s about the waiting status
            else if (ros::Time::now().toSec() - prev_time > 0.2f)
            {
                ROS_INFO("Waiting for replan");
                prev_time = ros::Time::now().toSec();
            }
        }
        if (robot_trajectory.trajectory.size() == 1)
        {
            ROS_INFO("Executed all planned path succesfully");
            robot_trajectory.trajectory.erase(robot_trajectory.trajectory.begin());
            waypoint_robot_state.erase(waypoint_robot_state.begin());
            traj_start_state_msg.erase(traj_start_state_msg.begin());
            executing_path.poses.erase(executing_path.poses.begin());
            executing = false;
            execution_flag = false;
            // Finish the current stage
            stages.erase(stages.begin());
            int sleep_time = rand() % 5;
            if (!stages.empty())
            {
                // Sleep for 5-10 secs before waking up
                ROS_INFO("Sleep for %d seconds", 5 + sleep_time);
                ros::Duration(5 + sleep_time).sleep();
                ROS_INFO("Woke up");
                // Back to planning for the next stages
                geometry_msgs::PoseArray dummy;
                pathPlanner(dummy);
                // And execute
                std_msgs::UInt32 command;
                command.data = 1;
                robotCommand(command);
            }
        }
        else
        {
            robot_trajectory.trajectory.erase(robot_trajectory.trajectory.begin());
            waypoint_robot_state.erase(waypoint_robot_state.begin());
            traj_start_state_msg.erase(traj_start_state_msg.begin());
            executing_path.poses.erase(executing_path.poses.begin());
            ROS_INFO("Success in the last action, try next one");
            movegroup_ptr->asyncExecute(robot_trajectory.trajectory.front());
        }
        break;
    default:
        ROS_INFO("MoveItErrorCodes: %d", result.result.error_code.val);
        break;
    }
}
// Helper function
// This helper function will return a list of trajectory indices in ascending order of categories
// (categories mentioned in replan() function)
std::vector<int> Planner::vectorsort(std::vector<int> vector)
{
    std::vector<std::pair<int, int>> a;
    std::vector<int> out;
    for (int i = 0; i < vector.size(); i++)
    {
        a.push_back(std::make_pair(vector[i], i));
    }
    std::sort(a.begin(), a.end());
    for (int i = 0; i < vector.size(); i++)
    {
        out.push_back(a[i].second);
    }
    return out;
}
// Unity State Update
void Planner::unityStateUpdate(const ros::TimerEvent &event)
{
    static const uint32_t IDLE = 1;
    static const uint32_t EXECUTING = 2;
    static const uint32_t TEMP_STOP = 3;
    static const uint32_t PREVIEW_SHOWING = 5;
    std_msgs::UInt32 unity_state;
    if ((executing && execution_flag))
    {
        unity_state.data = EXECUTING;
    }
    else if (!executing && execution_flag)
    {
        unity_state.data = TEMP_STOP;
    }
    else
    {
        unity_state.data = IDLE;
    }
    unityStatePublisher.publish(unity_state);
}
void Planner::robotCommand(std_msgs::UInt32 command)
{
    static const uint32_t EXECUTING = 1;
    static const uint32_t STOP = 2;
    static const uint32_t READY_STATE = 3;
    switch (command.data)
    {
    case EXECUTING:
        if (robot_trajectory.trajectory.size() > 0 && !execution_flag)
        {
            ROS_INFO("Execution Permission granted");
            execution_flag = true;
            movegroup_ptr->asyncExecute(robot_trajectory.trajectory.front());
            executing = true;
        }
        else
        {
            ROS_INFO("No trajectory has been added for execution");
        }
        break;
    case STOP:
        ROS_INFO("Robot termination");
        movegroup_ptr->stop();
        break;
    case READY_STATE:
        ROS_INFO("Bringing back to ready state");
        // First check if the robot is still running
        if (executing)
        {
            ROS_INFO("Robot is executing another trajectory, try later");
            return;
        }
        else
        {
            planning = true;
            planning_scene_monitor::LockedPlanningSceneRO lscene(psm);
            robot_state::RobotState ready_goal_state = robot_state::RobotState(lscene->getCurrentState());
            ready_goal_state.setToDefaultValues(joint_model_group, "ready");
            // Dummy stage variable for the executionStatus function to clear
            geometry_msgs::PoseArray dummy;
            stages.push_back(dummy);
            robot_state::robotStateToRobotStateMsg(robot_state::RobotState(lscene->getCurrentState()), req.start_state);
            moveit_msgs::Constraints state_goal =
                kinematic_constraints::constructGoalConstraints(ready_goal_state, joint_model_group);
            req.goal_constraints.clear();
            req.goal_constraints.push_back(state_goal);
            planning_pipeline->generatePlan(lscene, req, res);
            if (res.error_code_.val != res.error_code_.SUCCESS)
            {
                ROS_INFO("Plan bringing back to ready state: Unsucessful, error code: %d", res.error_code_.val);
                planning = false;
            }
            else
            {
                ROS_INFO("Plan bringing back to ready state: Sucessful");
                moveit_msgs::MotionPlanResponse response;
                res.getMessage(response);
                planning = false;
                traj_start_state_msg.push_back(response.trajectory_start);
                robot_trajectory.trajectory.push_back(response.trajectory);
                trajectoryDisplay(robot_trajectory);
                std_msgs::UInt32 command;
                command.data = EXECUTING;
                robotCommand(command);
                ROS_INFO("Bring back to ready state request sent");
            }
        }
        break;
    default:
        ROS_INFO("Invalid command: %d", command.data);
        break;
    }
}
void Planner::trajectoryDisplay(moveit_msgs::DisplayTrajectory robot_trajectory)
{
    // moveit_visual_tools_ptr_->deleteAllMarkers();
    // for (int i = 0; i < robot_trajectory.trajectory.size(); i++)
    // {
    //     moveit_visual_tools_ptr_->publishTrajectoryLine(robot_trajectory.trajectory[i], joint_model_group);
    // }
    // moveit_visual_tools_ptr_->trigger();
}
int main(int argc, char **argv)
{
    ros::init(argc, argv, "path_planning_rviz");
    ros::AsyncSpinner spinner(0);
    spinner.start();
    ros::NodeHandle node_handle("");
    ros::NodeHandle node_handle_private("~");
    Planner myPlanner(&node_handle, &node_handle_private);
    ros::waitForShutdown();
    return 0;
}
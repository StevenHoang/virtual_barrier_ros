# Virtual Barrier ROS - ROS Sub-System for Virtual Barrier ROS
## Pre-requisites
Before using the system, there are a few things that you need to install:
- [ROS-Melodic](http://wiki.ros.org/melodic/Installation/Ubuntu): The sub-system is developed of ROS Melodic and is tested to be stable with.
- [MoveIt-Melodic](https://moveit.ros.org/install/source/): MoveIt must be installed from source to benefit from the new functions that being added. The instructions of how to installed MoveIt from source is provided in the link. To install the the melodic-devel, follow the instructions in the **Advance** section at the end of the link provided.
- [Customised panda_moveit_config](https://gitlab.com/StevenHoang/panda_moveit_config): a customised panda_moveit_config package with different controller configurations for the purpose of Virtual Barrier ROS.
- [libfranka & franka_ros](https://frankaemika.github.io/docs/installation_linux.html): since the system is developed for PANDA research robot (only for now, can be implemented with different robots), check if these two packages have been installed on the computer for controlling the robot.
- [rosbridge](http://wiki.ros.org/rosbridge_suite) : rosbridge server for the connection between HoloLens and ROS
- [vbr_rviz_plugin](https://gitlab.com/StevenHoang/vbr_rviz_plugin) (optional): additional panels + tools for using Virtual Barrier system on ROS, mainly used for the experiment
## Environment Setup
Apart from the default workspace (/opt/ros/melodic), there are two more workspaces that need to be created. 
- MoveIt workspace: this is the workspace created after installing MoveIt from source using the instructions provided above in the link. Make sure DO NOT CHANGE any of the elements in this package as future users might want to use a clean version of MoveIt
- Virtual Barrier workspace: this is the workspace for the rest of the packages (customised panda_moveit_config, vbr_rviz_plugin and virtual_barrier_ros). This workspace can be created with any workspace creating tool (preferably wstool as instructed in MoveIt source installation). Clone all the packages into the source folder and build the workspace
- **Workspace Configuration**:
    - The Virtual Barrier workspace must be laid on top of the MoveIt workspace since Virtual Barrier uses MoveIt as a base for its functionalities.
    - MoveIt should be laid on top of the default workspace (/opt/ros/melodic)
    - To check the order of the workspace, use
    ```
    nano ~/.bashrc
    ```
    - To overlay the workspace, add the two following lines into the .bashrc file
    ```
    source ~/{YOUR_MOVEIT_WORKSPACE}/devel/setup.bash
    source ~/{YOUR_VIRTUAL_BARRIER_WORKSPACE}/devel/setup.bash
    ```
## Usage
To use the package with HoloLens, launch the following files in order
```
roslaunch rosbridge_server rosbridge_websocket.launch
roslaunch panda_moveit_config panda_control_moveit_rviz.launch robot_ip:={YOUR_ROBOT_IP}
roslaunch virtual_barrier_ros path_planning.launch
```
